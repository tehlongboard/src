#Written by Mario Garewal-Villarreal; 11/21/2019
#Just setting up the sensor to function when connected to the RPi
#Going to have to test functionality and add a buzzer/alarm based on distance



import RPi.GPIO as GPIO
import time

GPIO.setmode(GPIO.BCM)

#There are the Trig and Echo connections on the HC-SR404
#1 and 2 are placeholders; these numbers will be the GPIO Ports on Pi chosen
GPIO_TRIG = 1
GPIO_ECHO = 2

#Here are our GPIO input/output directions
GPIO.setup(GPIO_TRIG, GPIO.OUT)
GPIO.setup(GPIO_ECHO, GPIO.IN)

#Set time to gather times for start and end; when wave sent and then recieved
#Sonic wave is fast; why only set high for .01ms
def distance():
    GPIO.output(GPIO_TRIG, True)

    time.sleep(0.00001)
    GPIO.output(GPIO_TRIG, False)

    Start = time.time()
    Stop = time.time()

    while GPIO.input(GPIO_ECHO)== 0:
        Start = time.time()

    while GPIO.input(GPIO_ECHO)== 1:
        Stop = time.time()

        Time = Stop - Start
        #So, we have to muiltiply the time by the sonic speed, 34300 cm/s
        #Then we have to divide by 2 since the sensor gets the time on the wave
        #back. 
        distance = (Time * 34300) / 2

        return distance

if __name__ == '__main__':
    try:
        while True:
            dist = distance()
            #Could always convert it to other units if need be
            print ("Distance = %.1f cm" % dist)
            time.sleep(1)
 
        #Reset; CTRL + C like Rpi Lab
    except KeyboardInterrupt:
        print("Measurement stopped")
        GPIO.cleanup()